Proyecto calculadora Spring-Boot

Se crea una calculadora API Rest que soporta dos operaciones inicialmente suma y resta

Para su utilizacion se llama a una API REST GET /calculadora/calcula?operacion=suma&operando-uno=1&operando-dos=2)

Pasos para usar el proyecto

-Descargar el codigo
	git clone https://gitlab.com/JesVelBel/calculadora.git
-Instalarlo
	cd calculadora
	mvn clean install
-Arrancarlo
	java -jar target/calculadora-1.0.0.jar
-Probar
	Conectandose a http://localhost:8080/calculadora/calcula
	Se tiene una operacion GET , que se puede llamar con los parametros
	operacion  que puede tomar los valores suma o resta
	operando-uno, operando-2 que son los valores a utilizar
	Ejemplo
	http://localhost:8080/calculadora/calcula?operacion=suma&operando-uno=1&operando-dos=2