package es.minsait.calculadora.service;

import java.math.BigDecimal;


public interface CalculadoraService {

  BigDecimal opera(String operacion, BigDecimal operandoUno, BigDecimal operandoDos) throws Exception;

}
