package es.minsait.calculadora.service.impl;

import java.math.BigDecimal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import es.minsait.calculadora.service.CalculadoraService;

@Service
public class CalculadoraServiceImpl implements CalculadoraService {

  @Override
  public BigDecimal opera(String operacion, BigDecimal operandoUno, BigDecimal operandoDos) throws Exception {

    final Logger LOGGER = LoggerFactory.getLogger(CalculadoraService.class);

    LOGGER.debug("Operacion a efectuar :");

    switch (operacion) {
      case "suma":
        return operandoUno.add(operandoDos);
      case "resta":
        return operandoUno.subtract(operandoDos);
      default:
        LOGGER.error("No se puede utilizar esta operacion: {}", operacion);
        throw new Exception("No se puede utilizar esta operacion: " + operacion);

    }

  }

}
