package es.minsait.calculadora.controller;

import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import es.minsait.calculadora.service.CalculadoraService;
import io.corp.calculator.TracerAPI;

@RestController
@RequestMapping("/calculadora")
public class CalculadoraController {

  @Autowired
  private CalculadoraService calculadoraService;

  @GetMapping(value = "/calcula")
  public ResponseEntity<Object> calcular(@RequestParam(name = "operacion") String operacion,
      @RequestParam(name = "operando-uno") BigDecimal operandoUno,
      @RequestParam(name = "operando-dos") BigDecimal operandoDos) {

    BigDecimal result;

    try {
      result = calculadoraService.opera(operacion, operandoUno, operandoDos);
    } catch (Exception e) {
      return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_ACCEPTABLE);
    }

    return new ResponseEntity<>(result, HttpStatus.OK);
  }

}

