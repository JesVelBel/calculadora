package es.minsait.calculadora.service;

import java.math.BigDecimal;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import es.minsait.calculadora.service.impl.CalculadoraServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CalculadoraService.class)
public class CalculadoraServiceUnitTest {

  private final CalculadoraService calculadoraService = new CalculadoraServiceImpl();

  @Test
  public void testCalculaSumaOk() throws Exception {

    BigDecimal resultadoEsperado = BigDecimal.valueOf(8);

    BigDecimal result =
        calculadoraService.opera("suma", BigDecimal.valueOf(5), BigDecimal.valueOf(3));

    Assert.assertEquals(resultadoEsperado, result);

  }

  @Test
  public void testOperaRestaOK() throws Exception {

    BigDecimal resultadoEsperado = BigDecimal.valueOf(2);

    BigDecimal result =
        calculadoraService.opera("resta", BigDecimal.valueOf(5), BigDecimal.valueOf(3));

    Assert.assertEquals(resultadoEsperado, result);
  }

}

