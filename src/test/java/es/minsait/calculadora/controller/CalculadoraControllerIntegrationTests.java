package es.minsait.calculadora.controller;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import es.minsait.calculadora.CalculadoraApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CalculadoraApplication.class,
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CalculadoraControllerIntegrationTests {

  
  @LocalServerPort
  int puerto;
  
  private ResponseEntity<BigDecimal> opera(String operacion, BigDecimal operandoUno,
      BigDecimal operandoDos) throws URISyntaxException {
    RestTemplate restTemplate = new RestTemplate();

    final String baseUrl =
        "http://localhost:"+ puerto + "/calculadora/calcula?operacion=" + operacion
            + "&operando-uno=" + operandoUno + "&operando-dos=" + operandoDos;
    URI uri = new URI(baseUrl);

    ResponseEntity<BigDecimal> resultado = restTemplate.getForEntity(uri, BigDecimal.class);
    return resultado;
  }

  @Test
  public void testCalculaSumaOk() throws URISyntaxException {

    ResponseEntity<BigDecimal> resultado =
        opera("suma", BigDecimal.valueOf(5), BigDecimal.valueOf(3));

    Assert.assertEquals(200, resultado.getStatusCodeValue());
    Assert.assertEquals(BigDecimal.valueOf(8), resultado.getBody());
  }

  @Test
  public void testCalculaSumaOK() throws URISyntaxException {

    ResponseEntity<BigDecimal> resultado =
        opera("resta", BigDecimal.valueOf(5), BigDecimal.valueOf(3));

    Assert.assertEquals(200, resultado.getStatusCodeValue());
    Assert.assertEquals(BigDecimal.valueOf(2), resultado.getBody());
  }

}
